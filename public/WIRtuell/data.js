var APP_DATA = {
  "scenes": [
    {
      "id": "0-ideenwerk-lab---o14",
      "name": "Ideenwerk Lab - O1.4",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.7965933476883293,
        "pitch": 0.03465194151652895,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": -0.9579379516779056,
          "pitch": 0.5292465617632303,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": -0.82830136535242,
          "pitch": -0.03853777129934599,
          "rotation": 0.7853981633974483,
          "target": "2-flur---o16"
        },
        {
          "yaw": -1.1280962047220022,
          "pitch": -0.0226493212227048,
          "rotation": 0.7853981633974483,
          "target": "3-ideenwerk-makerspace2---o11"
        },
        {
          "yaw": -1.2930146211181075,
          "pitch": -0.03390680942825952,
          "rotation": 5.497787143782138,
          "target": "4-ideenwerk-makerspace1---o13"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.916360136633541,
          "pitch": 0.4970850534672113,
          "title": "<div>Hier geht's zum virtuellen Gundriss-Qube.</div>"
        }
      ]
    },
    {
      "id": "1-grundrissqube",
      "name": "GrundrissQube",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1320,
      "initialViewParameters": {
        "yaw": 0.7463444166653197,
        "pitch": 0.5002874056448761,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": 1.608308551047931,
          "pitch": 0.45175026911662286,
          "rotation": 0,
          "target": "0-ideenwerk-lab---o14"
        },
        {
          "yaw": 1.5245315546350566,
          "pitch": 0.13203759406548699,
          "rotation": 0,
          "target": "2-flur---o16"
        },
        {
          "yaw": 1.2309268016558548,
          "pitch": 0.476357740666689,
          "rotation": 0,
          "target": "4-ideenwerk-makerspace1---o13"
        },
        {
          "yaw": 1.232948637958664,
          "pitch": 0.014348882087203307,
          "rotation": 0,
          "target": "3-ideenwerk-makerspace2---o11"
        },
        {
          "yaw": 1.7844120782952109,
          "pitch": 0.04299313600947485,
          "rotation": 0,
          "target": "5-kche---o12"
        },
        {
          "yaw": 1.5379611310946535,
          "pitch": -0.28410715792571395,
          "rotation": 0,
          "target": "8-zwischenraum---o17a"
        },
        {
          "yaw": 1.8230455977678668,
          "pitch": -0.27925368604281786,
          "rotation": 0,
          "target": "7-knstlerinnen-garderobe---o17b"
        },
        {
          "yaw": -0.15243108499856461,
          "pitch": 0.45724846065062863,
          "rotation": 0,
          "target": "9-saal---e03"
        },
        {
          "yaw": -0.1661506037840681,
          "pitch": 0.20002912505252013,
          "rotation": 0,
          "target": "11-foyer-hinten---e06b"
        },
        {
          "yaw": 0.2105577114175965,
          "pitch": 0.08581288472679915,
          "rotation": 0,
          "target": "10-foyer-vorne---e06a"
        },
        {
          "yaw": -0.44599285354023266,
          "pitch": -0.26804315685784097,
          "rotation": 0,
          "target": "12-multifunktionsraum---e01"
        },
        {
          "yaw": 2.762848395434661,
          "pitch": 0.45972092909253703,
          "rotation": 0,
          "target": "13-heizung---ug"
        },
        {
          "yaw": 2.6913695291524835,
          "pitch": -0.6195688259412062,
          "rotation": 0,
          "target": "14-freifunk-eingang---ug"
        },
        {
          "yaw": -2.903270704538972,
          "pitch": 0.48414706699792376,
          "rotation": 0,
          "target": "15-telefon-apl---ug"
        },
        {
          "yaw": 2.7118577767182206,
          "pitch": -0.42763861044265283,
          "rotation": 0,
          "target": "16-freifunk-space-vorne---ug"
        },
        {
          "yaw": 2.7159797793288405,
          "pitch": -0.20787323651542877,
          "rotation": 0,
          "target": "17-freifunk-space-hinten---ug"
        },
        {
          "yaw": 2.6687541603403933,
          "pitch": 0.06267059266142994,
          "rotation": 0,
          "target": "18-freifunk-serverecke---ug"
        },
        {
          "yaw": 0.771413948274045,
          "pitch": 0.1732763513513529,
          "rotation": 0,
          "target": "19-eingang---auen"
        },
        {
          "yaw": 0.8373141310344678,
          "pitch": 0.3556629343611917,
          "rotation": 0,
          "target": "20-wareplatz---auen"
        },
        {
          "yaw": 0.45294619510310774,
          "pitch": -0.84216106462328,
          "rotation": 0,
          "target": "21-brunnen-im-hinterhof---auen"
        },
        {
          "yaw": -1.6117191312901973,
          "pitch": 0.1327342073972737,
          "rotation": 0,
          "target": "22-flur---o26"
        },
        {
          "yaw": -1.88365134945645,
          "pitch": 0.08371505024526016,
          "rotation": 0,
          "target": "23-aufnahmestudio---o21"
        },
        {
          "yaw": -1.752213597845703,
          "pitch": 0.48057026254275215,
          "rotation": 0,
          "target": "24-konferenzraum---o23"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-flur---o16",
      "name": "Flur - O1.6",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.26835611472660403,
        "pitch": 0.1265258168228609,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 0.15996993326931985,
          "pitch": -0.055452621521437706,
          "rotation": 5.497787143782138,
          "target": "0-ideenwerk-lab---o14"
        },
        {
          "yaw": -0.7366552412545442,
          "pitch": -0.07950800590797336,
          "rotation": 5.497787143782138,
          "target": "5-kche---o12"
        },
        {
          "yaw": 0.2532248602435274,
          "pitch": 0.6289017077669428,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 1.1956932212374767,
          "pitch": 0.3103463431504032,
          "rotation": 2.356194490192345,
          "target": "10-foyer-vorne---e06a"
        },
        {
          "yaw": -3.1110838513780585,
          "pitch": -0.5331455502241926,
          "rotation": 12.566370614359176,
          "target": "22-flur---o26"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-ideenwerk-makerspace2---o11",
      "name": "Ideenwerk Makerspace.2 - O1.1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 3.044509808669215,
        "pitch": -0.030318818464042607,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": 2.848904136072467,
          "pitch": 0.0380010173428893,
          "rotation": 5.497787143782138,
          "target": "0-ideenwerk-lab---o14"
        },
        {
          "yaw": 2.987545598996028,
          "pitch": 0.4696865751347232,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 1.0833755440879518,
          "pitch": -0.007318548076604259,
          "rotation": 0.7853981633974483,
          "target": "8-zwischenraum---o17a"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.5817731255740881,
          "pitch": -0.12229029944418812,
          "title": "<a href='https://video.r3s.nrw/videos/local?languageOneOf=en&amp;languageOneOf=de&amp;s=2' target='new'>video.r3s.nrw</a>"
        }
      ]
    },
    {
      "id": "4-ideenwerk-makerspace1---o13",
      "name": "Ideenwerk Makerspace.1 - O1.3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.6111432798011336,
        "pitch": 0.03546406524102608,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": 2.561566270368994,
          "pitch": 0.10447508728422861,
          "rotation": 1.5707963267948966,
          "target": "0-ideenwerk-lab---o14"
        },
        {
          "yaw": 2.632125965278698,
          "pitch": 0.5355430669753254,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 2.4376961209209664,
          "pitch": -0.10130914653274559,
          "rotation": 4.71238898038469,
          "target": "3-ideenwerk-makerspace2---o11"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-kche---o12",
      "name": "Küche - O1.2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.2558476853090621,
          "pitch": -0.03782348779939504,
          "rotation": 1.5707963267948966,
          "target": "6-der-letzte-macht-das-licht-aus"
        },
        {
          "yaw": 0.008423333134684796,
          "pitch": 0.503500583877873,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 0.08860061984395173,
          "pitch": -0.13856175300716522,
          "rotation": 0.7853981633974483,
          "target": "7-knstlerinnen-garderobe---o17b"
        },
        {
          "yaw": -0.8355444143088295,
          "pitch": 0.07035021420790954,
          "rotation": 5.497787143782138,
          "target": "2-flur---o16"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-der-letzte-macht-das-licht-aus",
      "name": "Der letzte macht das Licht aus!",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.26269961148330623,
          "pitch": -0.04246885919986099,
          "rotation": 4.71238898038469,
          "target": "5-kche---o12"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-knstlerinnen-garderobe---o17b",
      "name": "Künstler:innen-Garderobe - O1.7b",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.5069230394380071,
        "pitch": 0.04331492689566119,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": -1.1745346031729689,
          "pitch": -0.02729471655181115,
          "rotation": 5.497787143782138,
          "target": "5-kche---o12"
        },
        {
          "yaw": -0.5069230394380071,
          "pitch": 0.04331492689566119,
          "rotation": 0.7853981633974483,
          "target": "8-zwischenraum---o17a"
        },
        {
          "yaw": -0.7640539553945,
          "pitch": 0.5147043890533869,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-zwischenraum---o17a",
      "name": "Zwischenraum - O1.7a",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.1484119826429549,
        "pitch": 0.08295131625549068,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 0.12539957119192735,
          "pitch": 0.09284147598253334,
          "rotation": 5.497787143782138,
          "target": "7-knstlerinnen-garderobe---o17b"
        },
        {
          "yaw": 2.142237114336715,
          "pitch": 0.13501507084592923,
          "rotation": 0.7853981633974483,
          "target": "3-ideenwerk-makerspace2---o11"
        },
        {
          "yaw": 0.9225243028054173,
          "pitch": 0.5697233466759251,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "9-saal---e03",
      "name": "Saal - E0.3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.6843931421228682,
        "pitch": -0.1580160312455341,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 1.9739705755538308,
          "pitch": -0.1657768604873997,
          "rotation": 0.7853981633974483,
          "target": "10-foyer-vorne---e06a"
        },
        {
          "yaw": 0.7022374008431811,
          "pitch": -0.2543189045282013,
          "rotation": 5.497787143782138,
          "target": "11-foyer-hinten---e06b"
        },
        {
          "yaw": 1.7681374992566026,
          "pitch": 0.33324583370249883,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "10-foyer-vorne---e06a",
      "name": "Foyer, vorne - E0.6a",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.545038914734299,
        "pitch": -0.3206416202217053,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 2.3994063650684527,
          "pitch": -0.20754817769928025,
          "rotation": 1.5707963267948966,
          "target": "11-foyer-hinten---e06b"
        },
        {
          "yaw": 1.7969697726768086,
          "pitch": -0.1915080277128709,
          "rotation": 5.497787143782138,
          "target": "9-saal---e03"
        },
        {
          "yaw": 2.4856151012468137,
          "pitch": 0.1824659358642684,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": -3.0406853864665315,
          "pitch": -0.5719096056792825,
          "rotation": 0.7853981633974483,
          "target": "2-flur---o16"
        },
        {
          "yaw": 1.525118969130694,
          "pitch": -0.1414086475463172,
          "rotation": 4.71238898038469,
          "target": "19-eingang---auen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-foyer-hinten---e06b",
      "name": "Foyer, hinten - E0.6b",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -3.1124665635660342,
        "pitch": -0.17209201659023776,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": 2.5468312923788208,
          "pitch": -0.09712753572092225,
          "rotation": 5.497787143782138,
          "target": "10-foyer-vorne---e06a"
        },
        {
          "yaw": -2.1483149581631586,
          "pitch": -0.020159900480676285,
          "rotation": 7.0685834705770345,
          "target": "9-saal---e03"
        },
        {
          "yaw": 3.1193313896939445,
          "pitch": 0.39015010475905143,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 0.5883831692104948,
          "pitch": -0.2686022022740726,
          "rotation": 5.497787143782138,
          "target": "12-multifunktionsraum---e01"
        },
        {
          "yaw": 0.9427226948346323,
          "pitch": -0.03975942322886539,
          "rotation": 3.9269908169872414,
          "target": "16-freifunk-space-vorne---ug"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-multifunktionsraum---e01",
      "name": "Multifunktionsraum - E0.1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.2800453457556173,
        "pitch": -0.2336539255058927,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": -1.259980845611942,
          "pitch": 0.26610116810703666,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": -0.49991661522516395,
          "pitch": -0.10470978375491313,
          "rotation": 5.497787143782138,
          "target": "11-foyer-hinten---e06b"
        },
        {
          "yaw": -2.30570561770082,
          "pitch": -0.3225988556216066,
          "rotation": 5.497787143782138,
          "target": "21-brunnen-im-hinterhof---auen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-heizung---ug",
      "name": "Heizung - UG",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.2634157847033514,
        "pitch": 0.15365627928814263,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": -2.4503038830994157,
          "pitch": 0.7059276848392901,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.2140314570671631,
          "pitch": -0.46866425138450296,
          "title": "<a href='https://codefreezr.gitlab.io/chaoslab/teso/' target='new'>Meiiiin Schatzzz!</a>",
          "text": "click to continue"
        }
      ]
    },
    {
      "id": "14-freifunk-eingang---ug",
      "name": "Freifunk, Eingang - UG",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.7268052123264468,
        "pitch": -0.03855103807064708,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": 0.6600343687619841,
          "pitch": 0.5249350205418857,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 0.39879894565260443,
          "pitch": 0.06260167999256083,
          "rotation": 0.7853981633974483,
          "target": "16-freifunk-space-vorne---ug"
        },
        {
          "yaw": -0.009725198862508222,
          "pitch": -0.3017913078391121,
          "rotation": 5.497787143782138,
          "target": "21-brunnen-im-hinterhof---auen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "15-telefon-apl---ug",
      "name": "Telefon, APL - UG",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0,
        "pitch": 0,
        "fov": 1.1565807220340465
      },
      "linkHotspots": [
        {
          "yaw": -0.40655518938634927,
          "pitch": 0.6560428206321696,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": -0.2971223630476416,
          "pitch": 0.5430218046218371,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.4097362654832786,
          "pitch": -0.048383998700138875,
          "title": "<a href='https://codefreezr.gitlab.io/chaoslab/lutz/'>Lutz defacing PoC</a>"
        }
      ]
    },
    {
      "id": "16-freifunk-space-vorne---ug",
      "name": "Freifunk, Space vorne - UG",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.17625756647723634,
        "pitch": -0.2912481066488386,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 0.20725417238624466,
          "pitch": -0.36087492578882774,
          "rotation": 0.7853981633974483,
          "target": "11-foyer-hinten---e06b"
        },
        {
          "yaw": -1.1605103778627903,
          "pitch": -0.23985659791408764,
          "rotation": 5.497787143782138,
          "target": "14-freifunk-eingang---ug"
        },
        {
          "yaw": 0.8186754309737267,
          "pitch": -0.09826834943500984,
          "rotation": 1.5707963267948966,
          "target": "17-freifunk-space-hinten---ug"
        },
        {
          "yaw": -0.18568152441271302,
          "pitch": 0.20871961323810773,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "17-freifunk-space-hinten---ug",
      "name": "Freifunk, Space hinten - UG#",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.6939203480315541,
        "pitch": -0.21246788107988301,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": -0.3005882212546993,
          "pitch": 0.02275716831461949,
          "rotation": 5.497787143782138,
          "target": "16-freifunk-space-vorne---ug"
        },
        {
          "yaw": 0.14961194838186032,
          "pitch": -0.16057308343494903,
          "rotation": 0.7853981633974483,
          "target": "11-foyer-hinten---e06b"
        },
        {
          "yaw": 1.6812246698423774,
          "pitch": -0.08230911929554807,
          "rotation": 0.7853981633974483,
          "target": "18-freifunk-serverecke---ug"
        },
        {
          "yaw": 0.7011396260410425,
          "pitch": 0.28344635854913314,
          "rotation": 9.42477796076938,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "18-freifunk-serverecke---ug",
      "name": "Freifunk, Serverecke - UG",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.4234817883598474,
        "pitch": 0.07892549986560482,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": -0.1430735124774145,
          "pitch": 0.028188783380233673,
          "rotation": 5.497787143782138,
          "target": "17-freifunk-space-hinten---ug"
        },
        {
          "yaw": 0.3056304134939989,
          "pitch": 0.5757194155014567,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "19-eingang---auen",
      "name": "Eingang - Außen",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.6373065753455833,
        "pitch": 0.11019319504753078,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 3.1388245295645136,
          "pitch": -0.1171363914135295,
          "rotation": 10.995574287564278,
          "target": "21-brunnen-im-hinterhof---auen"
        },
        {
          "yaw": 2.724272869710889,
          "pitch": 0.6083927242843767,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": -2.8868581715851054,
          "pitch": 0.3003728770439622,
          "rotation": 2.356194490192345,
          "target": "20-wareplatz---auen"
        },
        {
          "yaw": 2.6373065753455833,
          "pitch": 0.11019319504753078,
          "rotation": 5.497787143782138,
          "target": "10-foyer-vorne---e06a"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.6599482670023207,
          "pitch": -0.32995096477066355,
          "title": "<a href='https://wir-wuelfrath.de/' target='new'>WIR Homepage</a>"
        }
      ]
    },
    {
      "id": "20-wareplatz---auen",
      "name": "Wareplatz - Außen",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.2762956447481031,
        "pitch": -0.28886082708614147,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 0.17767833427691215,
          "pitch": -0.05797286380580502,
          "rotation": 5.497787143782138,
          "target": "19-eingang---auen"
        },
        {
          "yaw": 0.3819725265151863,
          "pitch": 0.20997358127339538,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "21-brunnen-im-hinterhof---auen",
      "name": "Brunnen im Hinterhof - Außen",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 3.0332956387163907,
        "pitch": -0.010661637123865475,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 2.0007480172521452,
          "pitch": -0.06241272987949387,
          "rotation": 18.06415775814132,
          "target": "19-eingang---auen"
        },
        {
          "yaw": 2.900382744320348,
          "pitch": 0.06466006026140292,
          "rotation": 2.356194490192345,
          "target": "14-freifunk-eingang---ug"
        },
        {
          "yaw": -2.224736716636123,
          "pitch": -0.14345963451583543,
          "rotation": 19.63495408493622,
          "target": "12-multifunktionsraum---e01"
        },
        {
          "yaw": 2.9189047299202944,
          "pitch": 0.48861395117795325,
          "rotation": 9.42477796076938,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "22-flur---o26",
      "name": "Flur - O2.6",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.189000012221463,
        "pitch": -0.31512133474147674,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 2.5488961626201014,
          "pitch": -0.1313618874019209,
          "rotation": 2.356194490192345,
          "target": "2-flur---o16"
        },
        {
          "yaw": 2.1400812342597506,
          "pitch": 0.1840187381350038,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 1.7550561522924255,
          "pitch": -0.4187042243331014,
          "rotation": 13.351768777756625,
          "target": "23-aufnahmestudio---o21"
        },
        {
          "yaw": 1.4386788355370337,
          "pitch": -0.2583111402840075,
          "rotation": 5.497787143782138,
          "target": "24-konferenzraum---o23"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "23-aufnahmestudio---o21",
      "name": "Aufnahmestudio - O2.1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.7123302014845532,
        "pitch": 0.07468648476650053,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": 1.7747009691937228,
          "pitch": -0.020994708041758514,
          "rotation": 7.0685834705770345,
          "target": "22-flur---o26"
        },
        {
          "yaw": 1.6656736763829718,
          "pitch": 0.5761862657862871,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        },
        {
          "yaw": 1.577922443988843,
          "pitch": -0.09354754273998367,
          "rotation": 5.497787143782138,
          "target": "24-konferenzraum---o23"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.5085790864538975,
          "pitch": -0.2041102683937801,
          "title": "<a href='https://codefreezr.gitlab.io/chaoslab/lutz/'>Lutz defacing PoC</a>",
          "text": "click to continue"
        }
      ]
    },
    {
      "id": "24-konferenzraum---o23",
      "name": "Konferenzraum - O2.3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.2071436944769847,
        "pitch": -0.18173405448377444,
        "fov": 1.0359997736521234
      },
      "linkHotspots": [
        {
          "yaw": -2.2044561810326133,
          "pitch": -0.20274053047580765,
          "rotation": 1.5707963267948966,
          "target": "23-aufnahmestudio---o21"
        },
        {
          "yaw": -2.3193697788769896,
          "pitch": -0.07916469050680419,
          "rotation": 4.71238898038469,
          "target": "22-flur---o26"
        },
        {
          "yaw": -2.22656816666729,
          "pitch": 0.31413431639777123,
          "rotation": 3.141592653589793,
          "target": "1-grundrissqube"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
