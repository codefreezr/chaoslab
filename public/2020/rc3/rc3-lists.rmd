---
title: "RC3-is-not-37C3"
author: "CodeFreezR"
date: "2021-01-09"
output: 
  html_document:
    css: "rc3-lists.css"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

</br>

### ToC

![](img/CodefrzR-Rc3.svg){width=20%}

</br>

1. [CodeFreezR Session Tipps]  
    [1. Tech & InfoSec]  
    [2. Corona Warn Apps & more]  
    [3. OpenData]  
    [4. Science]  
    [5. Empathy & Diversity]  
    [6. Teamwork & Cooperation]  
    [7. Ethics, Society & Politics]  
    [8. Art & Culture]  
    [9. About rC3 / CCC]  
1. [Studios and Streams]
1. [rC3 Event Teams]
1. [More C3 Teams]
1. [More Core]
1. [More CF]


</br>
</br>




### CodeFreezR Session Tipps

</br>

#### 1. Tech & InfoSec
- Device Fingerprint - The Elephant in the Background -by- Julian Fietkau &nbsp;&nbsp;&nbsp;(en, 45min)&nbsp;&nbsp;&nbsp; [ 
[![](i/twt.svg){width=2%}](https://twitter.com/DetlefBurkhardt/status/1343591125161037824?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/113142/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-113142-the_elephant_in_the_background){target="_blank"} | 
[![](i/yt.svg){width=2.5%}](https://youtu.be/T6UtEjUWXzM){target="_blank"} ]

- Rage Against Maschine Learning -by- Hendrik Heuer &nbsp;&nbsp;&nbsp;(en, 54min)&nbsp;&nbsp;&nbsp; [ 
[![](i/twt.svg){width=2%}](https://twitter.com/DetlefBurkhardt/status/1343714349458849792?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/891673/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-891673-rage_against_the_machine_learning){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/d8wVDESdpLI){target="_blank"} ]

- Hacking the Nintendo Game & Watch -by- Roth &nbsp;&nbsp;&nbsp;(en, 42min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/twt.svg){width=2%}](){target="_blank"} |  -->
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11527/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11527-hacking_the_nintendo_game_watch){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/rLT9c4Miesw){target="_blank"} ]


[TOC]
</br>

#### 2. Corona Warn Apps & more
- Die Geschichte der Corona-Warn-App -by- Roemheld &nbsp;&nbsp;&nbsp;(de, 60min)&nbsp;&nbsp;&nbsp; [
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/729538/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-729538-die_geschichte_der_corona_warn_app){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/OV0eik_kExo){target="_blank"} ]


[TOC]
</br>

#### 3. OpenData
- FragDenStaat für Hacker:innen -by- Max &nbsp;&nbsp;&nbsp;(de, 11min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1345037482316984322?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/90559/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-90559-fragdenstaat_fuer_hacker){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/RIZTAuIG7_0){target="_blank"} ]

- The Open Show - Große Premiere! &nbsp;&nbsp;&nbsp;(de, 30min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1344880368999129088?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/46314/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-46314-the_open_show_grosse_premiere){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/IVqkKCUb6wg){target="_blank"} ]

- Datenguide: Statistiken für alle! -by- Jockers &nbsp;&nbsp;&nbsp;(de, 40min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-2020-158-datenguide-statistiken-fr-alle-){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->


[TOC]
</br>

#### 4. Science
- Nach der Coronakrise ist mitten in der Klimakrise -by- Quaschning &nbsp;&nbsp;&nbsp;(de, 55min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1343580153323122688?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11371/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11371-nach_der_coronakrise_ist_mitten_in_der_klimakrise){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/HtVws-WKy98){target="_blank"} ]

- Globalisierung, Digitalisierung und die Wachstumsfrage -by- Paech &nbsp;&nbsp;&nbsp;(de, 70min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1343882666526060544?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11574/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11574-globalisierung_digitalisierung_und_die_wachstumsfrage){target="_blank"} ]

- Wie Nerds überleben, wenn pizza.de down ist -by- kritis & honkhase &nbsp;&nbsp;&nbsp;(de, 40min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11561-wie_nerds_uberleben_wenn_pizza_de_down_ist){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->


[TOC]
</br>

#### 5. Empathy & Diversity
- failed empathy -by- betalars &nbsp;&nbsp;&nbsp;(de, 70min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1343750871239323654?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/824289/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-824289-failed_empathy){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/9P5Xg8pytjQ){target="_blank"} ]


[TOC]
</br>

#### 6. Teamwork & Cooperation
- How to solve conflict in a community of equals -by- Sebrechts &nbsp;&nbsp;&nbsp;(en, 45min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-channels-2020-125-how-to-solve-conflict-in-a-community-of-equals){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->

- Demokratische Entscheidungen in Communitys -by- Seidel & Hacker &nbsp;&nbsp;&nbsp;(de, 61min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-600086-demokratische_entscheidungen_in_online_communitys){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->


[TOC]
</br>

#### 7. Ethics, Society & Politics
- Netzpolitischer Jahresrücblick -by- Beckedahl &nbsp;&nbsp;&nbsp;(de, 60min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1344830574112944128?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11453/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11453-der_netzpolitische_wetterbericht_2020){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/watch?v=ipi8ju2iZdQ){target="_blank"}
[youtube-en](https://www.youtube.com/watch?v=DEOiA23Bd1g&t=1391s){target="_blank"} ]

- This Is Not A Game (QAnon) -by- Vogelgesang &nbsp;&nbsp;&nbsp;(de, 64min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1343923000496816128?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11500/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11500-this_is_not_a_game_de){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/B0d1hEWS6bE){target="_blank"} ]

- Nazis in Games -by- EEL &nbsp;&nbsp;&nbsp;(en, 61min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DetlefBurkhardt/status/1343969332930473986?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11501/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11501-nazis_in_games){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/nGPSKvU2_DU){target="_blank"} ]

- Kein Filter für Rechts -by- Kommerell &nbsp;&nbsp;&nbsp;(de, 46min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-684689-kein_filter_fuer_rechts){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->

- Körperverletzung im Amt durch Polizeibeamt:innen -by- Singelnstein &nbsp;&nbsp;&nbsp;(de, 40min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11562-korperverletzung_im_amt_durch_polizeibeamt_innen){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->

- Die soziale Revolution im Anarchismus -by- Jo. Eibusch &nbsp;&nbsp;&nbsp;(de, 49min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-2-die_soziale_revolution_im_anarchismus){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->


[TOC]
</br>

#### 8. Art & Culture
- Operation Mindfuck v4.0 -by- blinry & bleeptrack &nbsp;&nbsp;&nbsp;(en, 72 min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1344100776415801344?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/501024/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-501024-operation_mindfuck_vol_4){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/ywYBT0xM7so){target="_blank"} ]

- Qualityland 2.0 -by- Kling &nbsp;&nbsp;&nbsp;(de, 31 min)&nbsp;&nbsp;&nbsp; [
[![](i/twt.svg){width=2%}](https://twitter.com/DBurkhrdt/status/1343315527079514112?s=20){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11595/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11595-lesung_qualityland_2_0){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/ftFR4ljW8hA){target="_blank"} ]


[TOC]
</br>

#### 9. About rC3 / CCC
- #rC3 Eröffnung -by- blubbel &nbsp;&nbsp;&nbsp;(de, 30 min)&nbsp;&nbsp;&nbsp; [ 
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11583-rc3_eroffnung){target="_blank"} | 
[![](i/am.png){width=2.5%}](https://amara.org/en/videos/cGWwLYUy08oG/info/rc3-rc3-eroffnung/){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->

- Spatial Chat like workadventure -by- psy, Dückert & Negrier &nbsp;&nbsp;&nbsp;(en, 45min)&nbsp;&nbsp;&nbsp; [
<!-- [![](i/ptlx.svg){width=4%}](){target="_blank"} |  -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-404041-lernos_on_air_spacial_chat_with_workadventure){target="_blank"} ] 
<!-- [![](i/yt.svg){width=2%}](){target="_blank"} -->

- Infrastructure Review -by- YSF &nbsp;&nbsp;&nbsp;(de, 96min)&nbsp;&nbsp;&nbsp; [ 
[![](i/twt.svg){width=2%}](https://twitter.com/DetlefBurkhardt/status/1343714349458849792?s=20){target="_blank"} | 
[cf-reslide](https://bit.ly/rc3-infra), 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/talk/11585/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/v/rc3-11585-infrastructure_review){target="_blank"} | 
[infrateam-slides](https://md.td00.de/p/infraslides#/){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://youtu.be/IDsQfQ31P5w){target="_blank"} ]


[TOC]
</br>
</br>


### Studios and Streams

1. **Main (rC1, rC2)** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/main){target="_blank"}
 ]

1. **aboutfuture** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/hacc){target="_blank"}
 ]

1. **Bitwäscherei** &nbsp;&nbsp;&nbsp; [ 
[![](i/www.png){width=2%}](https://www.bitwaescherei.ch/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/Z%C3%BCrich%20Bitw%C3%A4scherei){target="_blank"} | 
[![](i/twt.svg){width=2%}](https://twitter.com/bitwasch){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UCSwORjSoEarmWFZTc7zfxUg){target="_blank"}
 ]

1. **c-base**  &nbsp;&nbsp;&nbsp; [ 
[![](i/www.png){width=2%}](https://c-base.org/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/B%20c-base){target="_blank"} | 
[![](i/twt.svg){width=2%}](https://twitter.com/cbase){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UCcAD6gbVVY6x84Kbn_8kJqw){target="_blank"}
 ]

1. **Chaostudio - Hamburg** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/hamburg)
 ]

1. **ChaosTrawler Stubnitz/Gängewiertel** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/ChaosTrawler){target="_blank"}
 ]

1. **chaoswest** &nbsp;&nbsp;&nbsp; [ 
[![](i/www.png){width=2%}](https://chaoswest.tv/), 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/Chaos-West){target="_blank"} | 
<!-- [rc3.world](https://rc3.world/rc3/assembly/CWTV/),  -->
[![](i/twt.svg){width=2%}](https://twitter.com/ChaosWildWest){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UCtuSU6ohVJmxMqlFOWm1_iw){target="_blank"}
 ]

1. **Chaoszone TV** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/ChaosZone){target="_blank"} | 
<!-- [rc3.world](https://rc3.world/rc3/assembly/chaoszone/),  -->
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UCsfpJrx2csVoioxv6xrCqJA){target="_blank"}
 ]

1. **franconian.net** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/franconian%2Enet%20talks){target="_blank"}
<!-- [rc3.world](https://rc3.world/rc3/assembly/franconian/) -->
 ]

1. **KreaturWorks** &nbsp;&nbsp;&nbsp; [ 
n/a <!-- [![](i/voc.svg){width=2%}](https://streaming.media.ccc.de/rc3/kreaturworks) -->
 ]

1. **OpenInfrastructureOrbit - oio** &nbsp;&nbsp;&nbsp; [ 
<!-- [![](i/voc.svg){width=2%}](https://streaming.media.ccc.de/rc3/oio), -->
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/OpenInfrastructureOrbit){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/user/FREIFUNK2013){target="_blank"}
 ]

1. **rc3 Lounge** &nbsp;&nbsp;&nbsp; [ 
[![](i/twt.svg){width=2%}](https://twitter.com/c3lounge){target="_blank"} 
<!-- [mp3](http://lounge.rc3.io/radio.mp3){target="_blank"} -->
 ]

1. **Remote Rhein Ruhr Stage - r3s** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/MON%20r3s%20Rhein%20VHS){target="_blank"}
<!-- [rc3.world](https://rc3.world/rc3/assembly/r3s/) -->
 ]

1. **Restrealität** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/restrealitaet){target="_blank"}
<!-- [rc3.world](https://rc3.world/rc3/assembly/restrealitaet/) -->
 ]

1. **Sendezentrum** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/Sendezentrum){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UC6asN9P2a-SSCtL8urIPyTQ){target="_blank"}
 ]

1. **wikipaka** &nbsp;&nbsp;&nbsp; [ 
[![](i/www.png){width=2%}](https://verschwoerhaus.de/tag/wikipaka/){target="_blank"} | 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/WikiPaka){target="_blank"} | 
[fahrplan](https://cfp.verschwoerhaus.de/rc3-2020/schedule/){target="_blank"} | 
[![](i/twt.svg){width=2%}](https://twitter.com/wikipaka){target="_blank"}
 ]

1. **xHain** &nbsp;&nbsp;&nbsp; [ 
[![](i/voc.svg){width=2%}](https://media.ccc.de/c/rc3/xHain%20Berlin){target="_blank"}
 ]


[TOC]
</br>
</br>

### rC3 Event Teams
1. **SHOC** <!-- Shared Herald Operation Center -->
1. **Heaven** <!-- Angel Coordination -->
1. **Signal Angels** <!-- Q&A Organisations -->
1. **Lineproducer** <!-- Chief Coordination of the Event (Aufnahmeleitung) -->
1. **C3VOC** <!-- Video Operation Center -->
1. **c3lingo** <!-- Realtime Translation -->
1. **c3subtitle** <!-- Transcribe everything -->
1. **C3POC** <!-- Phone Operation Center Eventphone -->
1. **GSM** <!-- Mobile Operators -->
1. **Haecksen** <!-- Female Power into Chaos -->
1. **ChaosPat:Innen** <!-- Helping new Mentees thru the Chaos -->
1. **rc3Adventure** <!-- 2d world online -->
1. **rc3Lounge** <!-- Music from a lot of Cities -->
1. **Hub** <!-- Software for this Conference Django-based -->
1. **c3auti** <!-- Support for Authistic People -->
1. **C3IOC** <!-- Inclusion Operation Center for People with dissassiblies-->
1. **c3gelb** <!-- Hygiene Konzept -->
1. **chaospost** <!-- Messages -->
1. **rC3Infra** <!-- Team Infrastructure -->
1. **Review-of-Review** <!-- How This Infra Review work -->
1. **HNS** <!-- Harald News Shows -->


[TOC]
<br>
</br>

### More C3 Teams
<!--
@c3inclusion
info@c3gelb.de

FEM Master Control Room Ilmenau

@c3subtitle
c3subtitles.de

Engelsystem &nbsp;&nbsp;&nbsp;(engelsystem.de)
-->
1. CERT - The Doctor
1. WOC - Waffel Operation Center
1. STOC - Sticker Operation Center
1. ASSM - Assemblies
1. FOC - Fashion Operation Center, merch
1. c3-teleshop <!-- @C3Teleshop -->
1. KidSpace - Stuff for Kids
1. c3streaming
1. Infodesk
1. c3Pixelflut
1. c3burokratie
1. c3Ohrwurm



[TOC]
</br>
</br>

### More Core

1. **media.ccc.de** 
[![](i/www.png){width=2%}](https://media.ccc.de/){target="_blank"} | 
[recording](https://media.ccc.de/c/rc3){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UC2TXq_t06Hjdr2g_KdKpHQg){target="_blank"}

<!-- 
1. **rc3.world** 
[![](i/www.png){width=2%}](https://rc3.world/rc3/)
[info nach login](https://rc3.world/rc3/static/start/)

1. **All Assemblies**
[rc3.world nach login](https://rc3.world/rc3/assemblies/all)
-->

1. **Fahrplan**
[![](i/www.png){width=2%}](https://fahrplan.events.ccc.de/rc3/2020/Fahrplan/){target="_blank"} | 
[![](i/ptlx.svg){width=4%}](https://pretalx.com/rc3/schedule/){target="_blank"}

1. **rC3 Logo Generator**
[![](i/www.png){width=2%}](https://rc3.bleeptrack.de/){target="_blank"}

1. **The Herald News Show:** 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UC6fvfDyWpE1J2MeCBVZYC-g){target="_blank"}

1. **C3WOC - inkl. Waffelstream** 
[![](i/yt.svg){width=2%}](https://www.youtube.com/channel/UCY8Afb_iJRvZe2pKmqrzaFg){target="_blank"} | 
<!-- [rc3.world](https://rc3.world/rc3/assembly/c3woc/) -->

1. **bleeptrack (Mindfuck/Generators)**
[![](i/www.png){width=2%}](https://bleeptrack.de/){target="_blank"} | 
[![](i/twt.svg){width=2%}](https://twitter.com/Bleeptrack){target="_blank"} | 
[![](i/yt.svg){width=2%}](https://www.youtube.com/user/BleeptrackPodcast){target="_blank"}

1. **blinry (Mindfuck)**
[![](i/www.png){width=2%}](https://morr.cc/){target="_blank"} | 
[![](i/twt.svg){width=2%}](https://twitter.com/blinry){target="_blank"}

1. **heyitsme**
[![](i/yt.svg){width=2%}](https://www.youtube.com/user/LukOikake){target="_blank"}


[TOC]
</br>
</br>

### More CF

1. #rc3world Moment
[![](i/twt.svg){width=2%}](http://bit.ly/cf-rc3wrld)

1. **social media wall**
[wall](https://my.walls.io/e3ef6)

1. **Infra Recap**
[pdf](https://bit.ly/rc3-infra)

1. **gitlab - chaoslab**
[repo](https://gitlab.com/codefreezr/chaoslab){target="_blank"}



[TOC]
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
























![](img/CodefrzR-Rc3.svg){width=10%}
