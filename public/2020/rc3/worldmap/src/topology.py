#!/usr/bin/env/python3

# A hacky script for a graph representation of the rC3 world by @alech
# CC-0

import json
import requests
import urllib.parse
import sys

edges = []

def full_url(curr_url, link_url):
	if 'https://' not in link_url:
		return urllib.parse.urljoin(curr_url, link_url)
	return link_url

def shorthost(netloc):
	return netloc.replace('.maps.at.rc3.world', '')

def shortname(url):
	u = urllib.parse.urlparse(url)
	n = '???'
	if u.path == '/main.json' or u.path == '//main.json':
		n = shorthost(u.netloc)
	elif '//maps' in u.path:
		n = shorthost(u.netloc) + '/' + u.path.replace('//maps/', '').replace('.json', '')
	elif '/maps' in u.path:
		n = shorthost(u.netloc) + '/' + u.path.replace('/maps', '').replace('.json', '')
	else:
		n = shorthost(u.netloc) + u.path.replace('.json', '')
	return n.replace('//', '/')

def get_edges(url, edges):
	print("# Looking at " + url + " (" + shortname(url) + ")", flush=True)
	j = requests.get(url).json()
	for l in j['layers']:
		exits = []
		try:
			for p in l['properties']:
				if p['name'] == 'exitUrl' or p['name'] == 'exitSceneUrl':
					# print("# found an exit: " + p['value'])
					exits.append(p['value'])
		except:
			pass	
		for e in exits:
			curr_shortname = shortname(url)
			new_url = full_url(url, e.strip())
			# print("# Full URL: " + new_url)
			new_shortname = shortname(new_url)
			if curr_shortname != new_shortname and (curr_shortname, new_shortname) not in edges:
				edges.append((curr_shortname, new_shortname))
				response = None
				try:
					response = requests.get(new_url)	
				except Exception as e:
					print("# Error requesting " + new_url + ": " + str(e))
				if response and response.status_code == 200:
					print('"' + curr_shortname + '" -> "' + new_shortname +'"', flush=True)
					if 'cert.maps.at.rc3.world/cube' in new_url:
						print("# CERT is crazy, nm")
					elif 'warsaw-hackerspace.maps.at.rc3.world/tictactoe' in new_url:
						print("# so is Warsaw hackerspace")
					else:
						get_edges(new_url, edges)
				else:
					print("# Request to " + new_url + " failed.")
					if response and response.status_code:
						print("# HTTP " + str(response.status_code))

# produces GraphViz DOT output, render with dot -Tpdf
print("strict digraph {\ngraph [bgcolor=black, ratio=0.5625];\n edge [color=\"#B239FF\"];\n node [color=\"#B239FF\", fontcolor=\"#B239FF\", fontname=\"Orbitron bold\", shape=box];")
get_edges('https://lobby.maps.at.rc3.world/main.json', edges)
print("}")
