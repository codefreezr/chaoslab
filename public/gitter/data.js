var APP_DATA = {
  "scenes": [
    {
      "id": "0-gitter",
      "name": "gitter",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [],
      "infoHotspots": [
        {
          "yaw": -0.7863388799874276,
          "pitch": -0.6104265987550264,
          "title": "<div>LinksOben</div>",
          "text": "Text"
        },
        {
          "yaw": 0.7918577850921089,
          "pitch": 0.6063930241669766,
          "title": "<div>RechtsUnten</div><div><br></div>",
          "text": "Text"
        },
        {
          "yaw": -0.7846992279343716,
          "pitch": 0.6087612468313548,
          "title": "<div>LinksUnten</div><div><br></div>",
          "text": "Text"
        },
        {
          "yaw": 0.7868949179333189,
          "pitch": -0.610533009061724,
          "title": "RechtsOben",
          "text": "Text"
        },
        {
          "yaw": 0,
          "pitch": 0,
          "title": "y:Null-p:Null",
          "text": "Text"
        },        
        {
          "yaw": 1,
          "pitch": 1,
          "title": "y:Eins-p:Eins",
          "text": "Text"
        }
      ]
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
