var APP_DATA = {
  "scenes": [
    {
      "id": "0-groundqube",
      "name": "Ground.Qube",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2000,
      "initialViewParameters": {
        "yaw": -0.02541029988902821,
        "pitch": 0.4065304586307281,
        "fov": 1.2724960246179906
      },
      "linkHotspots": [
        {
          "yaw": -1.5131054103068298,
          "pitch": -0.8685679192158844,
          "rotation": 0,
          "target": "20-unreal4"
        },
        {
          "yaw": -1.4137116058740347,
          "pitch": -0.8617702412599684,
          "rotation": 3.141592653589793,
          "target": "21-unreal5"
        },
        {
          "yaw": -2.8195322702385113,
          "pitch": -1.1064124353983367,
          "rotation": 0,
          "target": "17-qubelaborgitter"
        },
        {
          "yaw": -3.096912879916909,
          "pitch": -1.1028392135789673,
          "rotation": 0,
          "target": "18-qubelabortestbild"
        },
        {
          "yaw": 2.910916821865418,
          "pitch": -1.0666848413826582,
          "rotation": 0,
          "target": "19-qubelaborbasics"
        },
        {
          "yaw": -0.3129824551539997,
          "pitch": -0.10778114479715661,
          "rotation": 0,
          "target": "5-eg-saal3lowres"
        },
        {
          "yaw": -3.085791836881965,
          "pitch": -0.4041435609929529,
          "rotation": 0,
          "target": "16-og2-saal1qube"
        },
        {
          "yaw": -2.8849447386696205,
          "pitch": -0.28553046819591366,
          "rotation": 0,
          "target": "11-og2-saal1rand2"
        },
        {
          "yaw": -2.9878579142845823,
          "pitch": -0.43821768336168887,
          "rotation": 0,
          "target": "12-og2-saal1rand1"
        },
        {
          "yaw": -2.965312182699728,
          "pitch": -0.3566839375544628,
          "rotation": 0,
          "target": "13-og2-saal1rand3"
        },
        {
          "yaw": -3.036276730372178,
          "pitch": -0.24581064695810895,
          "rotation": 0,
          "target": "14-og2-saal1mitte"
        },
        {
          "yaw": -0.31393416355806636,
          "pitch": -0.27834497780335177,
          "rotation": 0,
          "target": "6-eg-saal3midres"
        },
        {
          "yaw": -0.5137973602927204,
          "pitch": 0.24257204577574143,
          "rotation": 0,
          "target": "8-eg-hallehxr-theatre"
        },
        {
          "yaw": 0.25910949343199974,
          "pitch": 0.27623978758369994,
          "rotation": 0,
          "target": "7-eg-halleh-pano"
        },
        {
          "yaw": 2.6701664092241284,
          "pitch": 0.11457872724354523,
          "rotation": 0,
          "target": "15-og2-foyerg-fpano"
        },
        {
          "yaw": 1.8108944322684133,
          "pitch": -0.12465229952214685,
          "rotation": 0,
          "target": "9-og1-garderobenfoyer-saal1pano"
        },
        {
          "yaw": 1.9412132328023537,
          "pitch": -0.03526088164791119,
          "rotation": 0,
          "target": "10-og1-rolltreppepano"
        },
        {
          "yaw": 0.3527978024583778,
          "pitch": -0.20336996108928318,
          "rotation": 0,
          "target": "4-eg-eingangshallepano4"
        },
        {
          "yaw": 0.45397372521829027,
          "pitch": -0.15923948099987584,
          "rotation": 0,
          "target": "3-eg-eingangshallepano3"
        },
        {
          "yaw": 0.544766357382592,
          "pitch": -0.10973297339748456,
          "rotation": 0,
          "target": "2-eg-eingangshallepano2"
        },
        {
          "yaw": 0.6277426076946657,
          "pitch": -0.0439947726059966,
          "rotation": 0,
          "target": "1-eg-eingangshallepano1"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.9965515590078535,
          "pitch": -0.961575802463793,
          "title": "Hier kommt noch ein Hyperlink hin",
          "text": "Ein Ausflug zu den <br>Wülfrather IdeenRäume.<br>"
        },
        {
          "yaw": 1.6779776472582322,
          "pitch": -0.9463247401169621,
          "title": "<a href='https://codefreezr.gitlab.io/chaoslab/lutz'>Defacing PoC mit Lutz (click)</a>",
          "text": "Click title to continue<br>"
        }
      ]
    },
    {
      "id": "1-eg-eingangshallepano1",
      "name": "EG-Eingangshalle.Pano1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 0.0033603808086191123,
        "pitch": -0.7304018993322359,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.23476374899389008,
          "pitch": -0.052771368733305124,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 0.8050123589467937,
          "pitch": -0.20513507712679058,
          "rotation": 1.5707963267948966,
          "target": "2-eg-eingangshallepano2"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-eg-eingangshallepano2",
      "name": "EG-Eingangshalle.Pano2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -0.062473467203988164,
        "pitch": 0.05873811219014158,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.23764515709256528,
          "pitch": 0.5837092844133522,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -0.15559788590018186,
          "pitch": 0.0037565667208205866,
          "rotation": 5.497787143782138,
          "target": "3-eg-eingangshallepano3"
        },
        {
          "yaw": -0.9582223467541784,
          "pitch": 0.185064675761339,
          "rotation": 4.71238898038469,
          "target": "1-eg-eingangshallepano1"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-eg-eingangshallepano3",
      "name": "EG-Eingangshalle.Pano3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.1695532566485003,
          "pitch": 0.6253712797147255,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -0.21925148089678714,
          "pitch": 0.008055343019965022,
          "rotation": 5.497787143782138,
          "target": "4-eg-eingangshallepano4"
        },
        {
          "yaw": -0.32874940007340925,
          "pitch": 0.28134519518278367,
          "rotation": 3.9269908169872414,
          "target": "2-eg-eingangshallepano2"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-eg-eingangshallepano4",
      "name": "EG-Eingangshalle.Pano4",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.1991329893265803,
          "pitch": 0.6447792950384539,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -0.10688143762676461,
          "pitch": 0.15733812197267483,
          "rotation": 11.780972450961727,
          "target": "10-og1-rolltreppepano"
        },
        {
          "yaw": -0.24530246543034906,
          "pitch": 0.2732042262072678,
          "rotation": 3.9269908169872414,
          "target": "3-eg-eingangshallepano3"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-eg-saal3lowres",
      "name": "EG-Saal3.LowRes",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 480,
      "initialViewParameters": {
        "yaw": -2.551566273735446,
        "pitch": -0.3133855805018193,
        "fov": 1.5877756795056996
      },
      "linkHotspots": [
        {
          "yaw": -2.6727387850628723,
          "pitch": 0.3663194601250801,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -2.6463234627534344,
          "pitch": 0.020597068353376713,
          "rotation": 1.5707963267948966,
          "target": "6-eg-saal3midres"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-eg-saal3midres",
      "name": "EG-Saal3.MidRes",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 3.120734604307531,
        "pitch": -0.20326484040117165,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.1207774349833386,
          "pitch": 0.41026661007414233,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 3.126060190566287,
          "pitch": -0.06422654259782945,
          "rotation": 4.71238898038469,
          "target": "5-eg-saal3lowres"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-eg-halleh-pano",
      "name": "EG-HalleH-Pano",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 0.6781958971974174,
        "pitch": 0.027538933338377092,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.25440875722180145,
          "pitch": -0.1199127121411685,
          "rotation": 5.497787143782138,
          "target": "8-eg-hallehxr-theatre"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-eg-hallehxr-theatre",
      "name": "EG-HalleH.XR-Theatre",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 512,
      "initialViewParameters": {
        "yaw": -0.008460606722895392,
        "pitch": -0.1092155472677252,
        "fov": 1.5877756795056996
      },
      "linkHotspots": [
        {
          "yaw": -0.02638245276688167,
          "pitch": 0.33180249913311854,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 0.6079499304047342,
          "pitch": -0.6525264163359239,
          "rotation": 0.7853981633974483,
          "target": "7-eg-halleh-pano"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "9-og1-garderobenfoyer-saal1pano",
      "name": "OG1-GarderobenFoyer-Saal1.Pano",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.015507624706337708,
          "pitch": 0.5688094100595631,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -0.4901466373872072,
          "pitch": -0.36079238445528006,
          "rotation": 5.497787143782138,
          "target": "16-og2-saal1qube"
        },
        {
          "yaw": -0.7819475403730856,
          "pitch": 0.32040192943614443,
          "rotation": 3.9269908169872414,
          "target": "10-og1-rolltreppepano"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "10-og1-rolltreppepano",
      "name": "OG1-Rolltreppe.Pano",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.298885679087757,
          "pitch": 0.021029401647393087,
          "rotation": 5.497787143782138,
          "target": "9-og1-garderobenfoyer-saal1pano"
        },
        {
          "yaw": -0.38182705273751694,
          "pitch": 0.4761142734116586,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -0.19818908103572497,
          "pitch": 0.1472707996093554,
          "rotation": 2.356194490192345,
          "target": "4-eg-eingangshallepano4"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-og2-saal1rand2",
      "name": "OG2-Saal1.Rand2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -0.15329601017823613,
        "pitch": 0.017448803130896806,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.04988653426543266,
          "pitch": 0.6954629859355173,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 0.835565870522462,
          "pitch": -0.011165258944810574,
          "rotation": 1.5707963267948966,
          "target": "16-og2-saal1qube"
        },
        {
          "yaw": -0.8102265518872649,
          "pitch": 0.4556494674889713,
          "rotation": 2.356194490192345,
          "target": "12-og2-saal1rand1"
        },
        {
          "yaw": 0.7161409371347318,
          "pitch": 0.23057052648418086,
          "rotation": 7.0685834705770345,
          "target": "14-og2-saal1mitte"
        },
        {
          "yaw": 0.5782622307611796,
          "pitch": 0.20166284159337522,
          "rotation": 3.9269908169872414,
          "target": "13-og2-saal1rand3"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-og2-saal1rand1",
      "name": "OG2-Saal1.Rand1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 0.34107999717348747,
        "pitch": 0.2960504739897196,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.5406578101216564,
          "pitch": -0.08125516898882701,
          "rotation": 0.7853981633974483,
          "target": "11-og2-saal1rand2"
        },
        {
          "yaw": 1.3157619233062707,
          "pitch": 0.057579717605637626,
          "rotation": 1.5707963267948966,
          "target": "16-og2-saal1qube"
        },
        {
          "yaw": -0.1922173189494636,
          "pitch": 0.8909805627749705,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 0.0881321403013331,
          "pitch": 0.10760949328585667,
          "rotation": 2.356194490192345,
          "target": "13-og2-saal1rand3"
        },
        {
          "yaw": 0.9098842780593301,
          "pitch": 0.20426542320455354,
          "rotation": 5.497787143782138,
          "target": "14-og2-saal1mitte"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-og2-saal1rand3",
      "name": "OG2-Saal1.Rand3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -0.06521753358221538,
        "pitch": -0.4537208301002309,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.3772782970070452,
          "pitch": -0.3272269750795562,
          "rotation": 7.853981633974483,
          "target": "14-og2-saal1mitte"
        },
        {
          "yaw": -0.9607639314333039,
          "pitch": -0.012194030234315534,
          "rotation": 3.9269908169872414,
          "target": "12-og2-saal1rand1"
        },
        {
          "yaw": -1.107348967808333,
          "pitch": -0.3623866088014047,
          "rotation": 5.497787143782138,
          "target": "11-og2-saal1rand2"
        },
        {
          "yaw": 0.9245824762606709,
          "pitch": -0.39340567301703366,
          "rotation": 1.5707963267948966,
          "target": "16-og2-saal1qube"
        },
        {
          "yaw": -0.08299882119300861,
          "pitch": 0.20433040167433347,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "14-og2-saal1mitte",
      "name": "OG2-Saal1.Mitte",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": 2.774895552605729,
        "pitch": 0.37502007127623216,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.594933386777668,
          "pitch": 0.3622404198727516,
          "rotation": 0.7853981633974483,
          "target": "11-og2-saal1rand2"
        },
        {
          "yaw": 1.9466901339693203,
          "pitch": 0.363506776746247,
          "rotation": 0.7853981633974483,
          "target": "12-og2-saal1rand1"
        },
        {
          "yaw": -2.8161446367947143,
          "pitch": 0.5463420435157964,
          "rotation": 1.5707963267948966,
          "target": "13-og2-saal1rand3"
        },
        {
          "yaw": 1.694348522874499,
          "pitch": 0.437998024476725,
          "rotation": 4.71238898038469,
          "target": "0-groundqube"
        },
        {
          "yaw": 2.437568618703633,
          "pitch": 0.9578963300694454,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "15-og2-foyerg-fpano",
      "name": "OG2-FoyerG-F.Pano",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -0.4088326340398112,
        "pitch": 0.48737820957319045,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.5754025810849939,
          "pitch": 1.0915923511337375,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "16-og2-saal1qube",
      "name": "OG2-Saal1.Qube",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2000,
      "initialViewParameters": {
        "yaw": -3.552713678800501e-15,
        "pitch": 0.00788886274206746,
        "fov": 1.4547062896383536
      },
      "linkHotspots": [
        {
          "yaw": -0.040316251812305026,
          "pitch": 0.6135469634061899,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 0.4566729184435889,
          "pitch": 0.0891462068497777,
          "rotation": 0.7853981633974483,
          "target": "11-og2-saal1rand2"
        },
        {
          "yaw": 0.32180198002031446,
          "pitch": 0.12771826970569045,
          "rotation": 0.7853981633974483,
          "target": "12-og2-saal1rand1"
        },
        {
          "yaw": 0.6438047729827012,
          "pitch": 0.15416236847567788,
          "rotation": 1.5707963267948966,
          "target": "13-og2-saal1rand3"
        },
        {
          "yaw": 0.5082710586664234,
          "pitch": 0.23812601861022564,
          "rotation": 1.5707963267948966,
          "target": "14-og2-saal1mitte"
        },
        {
          "yaw": 2.843616104037464,
          "pitch": 0.02562432672045034,
          "rotation": 3.9269908169872414,
          "target": "9-og1-garderobenfoyer-saal1pano"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "17-qubelaborgitter",
      "name": "Qube.Labor.Gitter",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.00037169629393218884,
          "pitch": 0.19796022523661883,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": 0.19471189183818893,
          "pitch": -9.85022197141916e-9,
          "rotation": 1.5707963267948966,
          "target": "18-qubelabortestbild"
        },
        {
          "yaw": -0.1681275629012724,
          "pitch": 0.0015937075190706196,
          "rotation": 4.71238898038469,
          "target": "19-qubelaborbasics"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.7801512732563793,
          "pitch": -0.6156387486735255,
          "title": "Links.Oben<br>",
          "text": "Text"
        },
        {
          "yaw": 0.7935042774431693,
          "pitch": -0.6123006605942543,
          "title": "<div>Rechts.Oben</div>",
          "text": "Text"
        },
        {
          "yaw": -0.0036152378712888122,
          "pitch": 0.007424322688397211,
          "title": "<div>Mitte.Mitte</div>",
          "text": "Text"
        },
        {
          "yaw": -0.7798463395197146,
          "pitch": 0.6172140245447277,
          "title": "<div>Links.Unten</div>",
          "text": "Text"
        },
        {
          "yaw": 0.7828333441971758,
          "pitch": 0.6187485852921917,
          "title": "<div>Rechts.unten</div>",
          "text": "Text"
        },
        {
          "yaw": -2.0776926651414698,
          "pitch": -1.5657831559833753,
          "title": "Mitte.Mitte (_up)<br>",
          "text": "Text"
        },
        {
          "yaw": -2.7485269109088115,
          "pitch": 1.567122281202769,
          "title": "Mitte.Mitte (_down)<br>",
          "text": "Text"
        },
        {
          "yaw": -1.5732364071662577,
          "pitch": 0.0036005356689603474,
          "title": "<div>Mitte.Mitte (_left)</div>",
          "text": "Text"
        },
        {
          "yaw": 1.5652281776826467,
          "pitch": -0.0007194554613718651,
          "title": "<div>Mitte.Mitte (_right)</div>",
          "text": "Text"
        },
        {
          "yaw": 3.139261844548801,
          "pitch": 0.0047576511372895425,
          "title": "<div>Mitte.Mitte (_back)</div>",
          "text": "Text"
        }
      ]
    },
    {
      "id": "18-qubelabortestbild",
      "name": "Qube.Labor.Testbild",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2000,
      "initialViewParameters": {
        "yaw": 0.007818553811995699,
        "pitch": 0.6070112327499579,
        "fov": 1.2724960246179906
      },
      "linkHotspots": [
        {
          "yaw": 0.22244572569938548,
          "pitch": 0.5673519629674271,
          "rotation": 1.5707963267948966,
          "target": "19-qubelaborbasics"
        },
        {
          "yaw": -0.2511011507092267,
          "pitch": 0.5657038338585103,
          "rotation": 4.71238898038469,
          "target": "17-qubelaborgitter"
        },
        {
          "yaw": -0.01003478655926493,
          "pitch": 0.6771671139984079,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "19-qubelaborbasics",
      "name": "Qube.Labor.Basics",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.08446612813427201,
          "pitch": 0.22527272225022443,
          "rotation": 4.71238898038469,
          "target": "18-qubelabortestbild"
        },
        {
          "yaw": 0.1062853818354963,
          "pitch": 0.2243995856054113,
          "rotation": 1.5707963267948966,
          "target": "17-qubelaborgitter"
        },
        {
          "yaw": 0.009698547866204166,
          "pitch": 0.315598627846704,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "20-unreal4",
      "name": "unreal4",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1536,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.5516422886865637,
          "pitch": -0.03899249145107575,
          "rotation": 1.5707963267948966,
          "target": "21-unreal5"
        },
        {
          "yaw": -0.014547234459946878,
          "pitch": 0.3601707064090913,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "21-unreal5",
      "name": "unreal5",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1536,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.0048493637548610025,
          "pitch": 0.3812652588473888,
          "rotation": 3.141592653589793,
          "target": "0-groundqube"
        },
        {
          "yaw": -0.7676071547352183,
          "pitch": -0.03023329374233441,
          "rotation": 4.71238898038469,
          "target": "20-unreal4"
        }
      ],
      "infoHotspots": []
    }


    ,








    {
      "id": "30-dreamscape",
      "name": "Dreamscape",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -2.153098457373341,
        "pitch": 0.6479732763754349,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.414630095398657,
          "pitch": 0.8154903544592536,
          "rotation": 16.493361431346422,
          "target": "31-deeper-underground"
        },
        {
          "yaw": 2.3893291471699527,
          "pitch": 0.7818440516914347,
          "rotation": 0,
          "target": "32-treasure-cave"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "31-deeper-underground",
      "name": "Deeper Underground",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.9010876638849403,
          "pitch": 0.32643332136289516,
          "rotation": 5.497787143782138,
          "target": "30-dreamscape"
        },
        {
          "yaw": 0.9065037231378952,
          "pitch": 0.2966575844374866,
          "rotation": 0.7853981633974483,
          "target": "32-treasure-cave"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "32-treasure-cave",
      "name": "Treasure Cave",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -0.8549169405913357,
        "pitch": 0.2940116221326896,
        "fov": 1.2638305905743639
      },
      "linkHotspots": [
        {
          "yaw": -1.1382856453045527,
          "pitch": 0.32824624472764086,
          "rotation": 0,
          "target": "31-deeper-underground"
        },
        {
          "yaw": 1.1300755114622785,
          "pitch": 0.32356904379469853,
          "rotation": 0,
          "target": "30-dreamscape"
        }
      ],
      "infoHotspots": []
    }




  ],
  "name": "CCH - Vorstudien",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
