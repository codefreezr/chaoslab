var APP_DATA = {
  "scenes": [
    {
      "id": "30-dreamscape",
      "name": "Dreamscape",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -2.153098457373341,
        "pitch": 0.6479732763754349,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.414630095398657,
          "pitch": 0.8154903544592536,
          "rotation": 16.493361431346422,
          "target": "31-deeper-underground"
        },
        {
          "yaw": 2.3893291471699527,
          "pitch": 0.7818440516914347,
          "rotation": 0,
          "target": "32-treasure-cave"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "31-deeper-underground",
      "name": "Deeper Underground",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.9010876638849403,
          "pitch": 0.32643332136289516,
          "rotation": 5.497787143782138,
          "target": "30-dreamscape"
        },
        {
          "yaw": 0.9065037231378952,
          "pitch": 0.2966575844374866,
          "rotation": 0.7853981633974483,
          "target": "32-treasure-cave"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "32-treasure-cave",
      "name": "Treasure Cave",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1024,
      "initialViewParameters": {
        "yaw": -0.8549169405913357,
        "pitch": 0.2940116221326896,
        "fov": 1.2638305905743639
      },
      "linkHotspots": [
        {
          "yaw": -1.1382856453045527,
          "pitch": 0.32824624472764086,
          "rotation": 0,
          "target": "31-deeper-underground"
        },
        {
          "yaw": 1.1300755114622785,
          "pitch": 0.32356904379469853,
          "rotation": 0,
          "target": "30-dreamscape"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
