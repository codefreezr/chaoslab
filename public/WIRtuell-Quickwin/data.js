var APP_DATA = {
  "scenes": [
    {
      "id": "0-img_51_709",
      "name": "IMG_51_709",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.9707762184567628,
        "pitch": -0.006192676525332175,
        "fov": 1.3364529992237493
      },
      "linkHotspots": [
        {
          "yaw": 2.842106336575471,
          "pitch": -0.029754404463282924,
          "rotation": 6.283185307179586,
          "target": "2-img_53_244"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-img_52_865",
      "name": "IMG_52_865",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.6190322044567402,
        "pitch": 0.006073986215088922,
        "fov": 1.3364529992237493
      },
      "linkHotspots": [
        {
          "yaw": 2.5073141320158543,
          "pitch": 0.004227421919376084,
          "rotation": 7.0685834705770345,
          "target": "2-img_53_244"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-img_53_244",
      "name": "IMG_53_244",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.783918693139114,
        "pitch": -0.016567070366996006,
        "fov": 1.3364529992237493
      },
      "linkHotspots": [
        {
          "yaw": -1.13208322964433,
          "pitch": -0.04237907757315007,
          "rotation": 0.7853981633974483,
          "target": "0-img_51_709"
        },
        {
          "yaw": -1.2826309610864701,
          "pitch": -0.03219716708259668,
          "rotation": 5.497787143782138,
          "target": "1-img_52_865"
        },
        {
          "yaw": -0.8173545679317122,
          "pitch": -0.09213827416126286,
          "rotation": 6.283185307179586,
          "target": "3-img_54_864"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-img_54_864",
      "name": "IMG_54_864",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.1165557195153557,
          "pitch": -0.7042821353189712,
          "rotation": 0,
          "target": "4-img_60_289"
        },
        {
          "yaw": 0.16008093115467048,
          "pitch": -0.10427561039205635,
          "rotation": 0,
          "target": "2-img_53_244"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-img_60_289",
      "name": "IMG_60_289",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.492502274480616,
          "pitch": -0.11038823661425212,
          "rotation": 11.780972450961727,
          "target": "3-img_54_864"
        },
        {
          "yaw": -2.2972145673611344,
          "pitch": -0.2744417570779554,
          "rotation": 0.7853981633974483,
          "target": "5-img_61_455"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-img_61_455",
      "name": "IMG_61_455",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.6624799089881872,
          "pitch": -0.23997254582949878,
          "rotation": 0,
          "target": "4-img_60_289"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "WIRtuell",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": true,
    "viewControlButtons": false
  }
};
