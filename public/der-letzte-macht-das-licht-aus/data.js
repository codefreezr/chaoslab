var APP_DATA = {
  "scenes": [
    {
      "id": "0-kche-licht-an",
      "name": "Küche Licht an",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.02837792100474701,
        "pitch": 0.08979019374736907,
        "fov": 1.1219225663959815
      },
      "linkHotspots": [
        {
          "yaw": 0.2691324256962062,
          "pitch": -0.04664936654322105,
          "rotation": 0,
          "target": "1-kche-licht-aus"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-kche-licht-aus",
      "name": "Küche Licht aus",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.02837792100474701,
        "pitch": 0.08979019374736907,
        "fov": 1.1219225663959815
      },
      "linkHotspots": [
        {
          "yaw": 0.2691324256962062,
          "pitch": -0.04664936654322105,
          "rotation": 0,
          "target": "0-kche-licht-an"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
