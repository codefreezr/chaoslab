var APP_DATA = {
  "scenes": [
    {
      "id": "0-unreal4",
      "name": "unreal4",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1536,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.9737936443077153,
          "pitch": -0.01664291206259172,
          "rotation": 4.71238898038469,
          "target": "1-unreal5"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.3204493383775553,
          "pitch": -0.2687618385168804,
          "title": "Created with &lt;3 and <br>",
          "text": "Panoramic Capture Tool in 4.26<br>"
        }
      ]
    },
    {
      "id": "1-unreal5",
      "name": "unreal5",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1536,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.9836238153550276,
          "pitch": -0.021430353459656715,
          "rotation": 1.5707963267948966,
          "target": "0-unreal4"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.5456645003750111,
          "pitch": -0.3559381159928279,
          "title": "Created with &lt;3 and <br>",
          "text": "Panoramic Capture Tool in 5.0.4<br>"
        }
      ]
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
