var APP_DATA = {
  "scenes": [
    {
      "id": "0-photo_2022-08-28_20-08-47",
      "name": "photo_2022-08-28_20-08-47",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [],
      "infoHotspots": [
        {
          "yaw": -1.065904889654636,
          "pitch": -0.5822299027111733,
          "title": "Hello",
          "text": "<div>panoWorld</div>"
        }
      ]
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
