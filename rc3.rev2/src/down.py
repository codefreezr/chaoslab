import requests

def getEvent(StageShort):
    #print (StageShort)
    url = "https://pretalx.c3voc.de/rc3-2021-" + StageShort + "/schedule/export/schedule.xml"
    fname = "rc3-2021\\xml\\" + StageShort + ".xml"
    print (url)
    response = requests.get(url)
    with open(fname, 'wb') as file:
        file.write(response.content)


getEvent("cbase")
getEvent("chaoszone")
getEvent("cwtv")
getEvent("fem")
getEvent("hacc-a-f")
getEvent("haecksen")
getEvent("gehacktes-from-hell")
getEvent("r3s")
getEvent("chill-lounge")
getEvent("lounge")
getEvent("sendezentrum")
getEvent("xhain")
getEvent("chaosstudiohamburg")