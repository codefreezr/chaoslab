import xmltodict, csv



def convertEvent(StageShort):
    
    input = "rc3-2021\\xcal\\" + StageShort + ".xml"
    output = "rc3-2021\\csv\\" + StageShort + ".csv"

    with open(input, encoding='utf-8') as xmlfile: # PARSE XML FILE
        xml = xmltodict.parse(xmlfile.read(), force_list={'vevent': True})

    
    csvfile = open(output,'w',encoding='utf-8',newline='') # CREATE CSV FILE
    csvfile_writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL, dialect=csv.excel_tab)


    csvfile_writer.writerow(["stage","lang","date","time","dura","url","category","location","summary","speaker","description"]) # ADD HEADER

    for event in xml["iCalendar"]["vcalendar"]["vevent"]: 
        print(event["pentabarf:title"])
        myDura = int(float(event["duration"]) * 1000)
        if myDura == 2000:
            myDura = 120
        elif myDura == 1030:
            myDura = 90
        elif myDura == 1050:
            myDura = 110
        elif myDura == 1000:
            myDura = 60
        elif myDura == 3000:
            myDura = 180            
        elif myDura == 3030:
            myDura = 210
        elif myDura == 2029:
            myDura = 150
        elif myDura == 4000:
            myDura = 240
        elif myDura == 1020:
            myDura = 80



        str = event["dtstart"]
        myDate = str[6:8]
        h = str[9:11]
        m = str[11:13]
        myTime = h + ":" + m

        if event["description"] is None:
            myDesc = "n/a"
        else:
            myDesc = event["description"].splitlines()

        if event["attendee"] is None:
            speaker = "n/a"
        else:
            speaker =  event["attendee"]

        csv_line = [StageShort,
                    event["pentabarf:language"],
                    myDate,
                    myTime,
                    myDura,
                    event["url"],
                    event["category"],
                    event["location"],
                    event["summary"],
                    speaker,
                    myDesc
                    ] # EXTRACT EMPLOYEE DETAILS
        csvfile_writer.writerow(csv_line) # ADD A NEW ROW TO CSV FILE


convertEvent("rc3-2021")

#convertEvent("cbase")
#convertEvent("chaoszone")
#convertEvent("fem")
#convertEvent("hacc-a-f")
#convertEvent("r3s")
#convertEvent("sendezentrum")
#convertEvent("xhain")
#convertEvent("chaosstudiohamburg")


#!! -> Müssen manuell gesäubert werden. doppelte attendee:
#convertEvent("chill-lounge")
#convertEvent("lounge")
#convertEvent("cwtv")
#convertEvent("haecksen")
#convertEvent("gehacktes-from-hell")