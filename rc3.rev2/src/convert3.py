import xmltodict, csv


def iterdict(d):
  for k,v in d.items():        
     if isinstance(v, dict):
         iterdict(v)
     else:            
         print (k,":",v)


def convertEvent(StageShort):
    
    input = "rc3-2021\\xml\\" + StageShort + ".xml"
    output = "rc3-2021\\csv2\\" + StageShort + ".csv"

    
    with open(input, encoding='utf-8') as xmlfile: # PARSE XML FILE
        xml = xmltodict.parse(xmlfile.read()) #, force_list={'event': True} || , force_list=('day','room','event')

    #print(xml)
    
    csvfile = open(output,'w',encoding='utf-8',newline='') # CREATE CSV FILE
    csvfile_writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL, dialect=csv.excel_tab)

    csvfile_writer.writerow(["slug","url","guid","id"]) # ADD HEADER


    for myday in xml["schedule"]["day"]:
        iterdict(xml["schedule"])


    '''
    for myday in xml["schedule"]["day"]: 

        print(myday["@index"])
        for k,v in myday.items():
            #print(k,":",v)
            if isinstance(v, dict):
                for i,n in v.items():
                    if isinstance(n, dict):
                        for r,m in n.items():
                            print(r,":",m)



        #print(myday["room"])

        #for myroom in myday["day"]["room"]:
            #print(myroom["@start"])
         #   a = 1
            #myroom.get('@name')
           #print(myroom["@name"])

       
        csv_line = [StageShort,
                    eve["slug"],
                    eve["url"],
                    eve["@guid"],
                    eve['@id']
                    ] # EXTRACT EMPLOYEE DETAILS
        csvfile_writer.writerow(csv_line) # ADD A NEW ROW TO CSV FILE
        '''

#convertEvent("cbase")
#convertEvent("chaoszone")
#convertEvent("fem")
convertEvent("hacc-a-f")
#convertEvent("r3s")
#convertEvent("sendezentrum")
#convertEvent("xhain")
#convertEvent("chaosstudiohamburg")


#!! -> Müssen manuell gesäubert werden. doppelte attendee:
#convertEvent("chill-lounge")
#convertEvent("lounge")
#convertEvent("cwtv")
#convertEvent("haecksen")
#convertEvent("gehacktes-from-hell")