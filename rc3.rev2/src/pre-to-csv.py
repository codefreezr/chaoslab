import xmltodict, csv





with open("rc3-2021\\xcal\\data.xml") as xmlfile: # PARSE XML FILE
    xml = xmltodict.parse(xmlfile.read())

csvfile = open("rc3-2021\\csv\\data2.csv",'w',encoding='utf-8',newline='') # CREATE CSV FILE
csvfile_writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL, dialect=csv.excel)

csvfile_writer.writerow(["name","role","age"]) # ADD HEADER

for employee in xml["employees"]["employee"]: #FOR EACH EMPLOYEE
    csv_line = [employee["name"], 
                employee["role"],
                employee["age"]] # EXTRACT EMPLOYEE DETAILS
    csvfile_writer.writerow(csv_line) # ADD A NEW ROW TO CSV FILE