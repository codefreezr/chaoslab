import xmltodict, csv

input = "schedule.xcal"
output = "schedule.csv"

with open(input, encoding='utf-8') as xmlfile: # PARSE XML FILE
    xml = xmltodict.parse(xmlfile.read(), force_list={'event': True})


csvfile = open(output,'w',encoding='utf-8',newline='') # CREATE CSV FILE
csvfile_writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL, dialect=csv.excel_tab)

csvfile_writer.writerow(["id","slug","title","subtitle","lang","track","type","location","start","duration","abstract","description","attendee","url"]) # ADD HEADER

i = 1
for event in xml['iCalendar']['vcalendar']['vevent']:
    print(str(i) + ": " + event["pentabarf:title"])
    i = i + 1

    myAttendee = "n/a"
    if 'attendee' in event:
        myAttendee = event["attendee"]

    csv_line = [
        
        event["pentabarf:event-id"],
        event["pentabarf:event-slug"],
        event["pentabarf:title"],
        event["pentabarf:subtitle"],
        event["pentabarf:language"],
        event["pentabarf:track"],
        event["categories"],
        event["location"],
        event["dtstart"],
        event["pentabarf:duration"],
        event["summary"],
        event["description"],
        myAttendee,
        event["url"]
    ]
    csvfile_writer.writerow(csv_line)

"""
csvfile_writer.writerow(csv_line)


for days in xml['schedule']['day']:
    for rooms in days["room"]:
        if 'event' in rooms:
            for events in rooms["event"]:
                print(str(i) + ": " + events["title"]) # print(events["@id"])
                i = i + 1

                csv_line = [StageShort,
                    event["pentabarf:language"],
                    myDate,
                    myTime,
                    myDura,
                    event["url"],
                    event["category"],
                    event["location"],
                    event["summary"],
                    speaker,
                    myDesc
                    ] # EXTRACT EMPLOYEE DETAILS
        csvfile_writer.writerow(csv_line)




for event in xml["schedule"]["day"]["room"]["event"]: 
    print(event["title"])

 # ADD A NEW ROW TO CSV FILE
 """
