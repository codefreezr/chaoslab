---
tags: 37c3, call4, divoCore
---

# Call for Qubes
*(english Version & Glossary see below)*

Gesucht: Federierte Zwischenräume, kurz Qubes.

Wir wollen Lösungen aus den Bereichen AR, VR, MR, kurz XR, miteinander vernetzen, idealerweise federiert. Dazu suchen wir deine / eure Lösung. Egal ob voll, halb oder 1/3 fertig. 


Wir denke da so in drei Grundarten von Qubes:

* **RealQubes** - aka VR-Caves oder XR-Stages
* **RemoteQubes** - VorOrt DIY VR/AR/XR Lösungen aus Hacker-, Makerspaces & Stages
* **VirtualQubes** - 2D / 3D CollabSpaces im Web oder als App

plus: **MetaQubes** - Qubes inside Qubes


### Was ist das bitte schön genau?
Gemeint sind Treffpunkte und Schnittstellen für Wesen, Ro|Bots, Tele|Wokas, Applikationen, Plattformen, Protokollen & Engines zwischen Cyber und Realraum.

> * **RealQubes:** Geeignet für größere Menschenmengen, z.b. auf OnSite-Verantstaltungen. XR-Stages skalieren dabei besser als die meisten Cave-Konzepte.

> * **RemoteQubes:** Kleinere ggf. DIY-Caves und/oder HW/SW VR/AR Lösungen optimalerweise mit Mounts, z.b. in/aus/für Hacker-/MakerSpaces, Erfas, RemoteStages. Geht hier FOSS?

> * **VirtualQubes:** Bei jedem zuhause am Bildschirm oder per VR-Glasses rezipierbar, ggf. auch auf Handheld / Tablet. Im Kern überwiegend als reine Software. Bitte bleibt FOSS.

> * **MetaQubes:** Qubes inside Qubes, wie Telepräsenz-Geräte, RealWesenTwins, Projektionsflächen, Steuereinheiten uvm. Sie gehören immer zu einem Basis-Qube.


### Ihr habt ....
... Talks, Präsentationen, Installation, Codes oder Konzepte?

### Wir überlegen gemeinsam ... 
... wie & ob wir Dinge federiert zum klingen bringen und den Cyber mit dem Realen verweben.

### Rein damit
* Reicht Eure Ideen noch heute hier ein:  [Pretalx Url]
* Einsendeschluß: [Datum]
* Fragen? Fragen! [Neutraler Matrix-Chatroom speziell für den Call]

### Glossary/Begriffe:
VR: [Virtual Reality, Wikipedia](https://en.wikipedia.org/wiki/Virtual_reality)
AR: [Augmented Reality, Wikipedia](https://en.wikipedia.org/wiki/Augmented_reality)
MR: [Mixed Reality, Wikipedia](https://en.wikipedia.org/wiki/Mixed_reality)
XR: [Extended Reality, Wikipedia](https://en.wikipedia.org/wiki/Extended_reality) (bringt AR, VR & MR zusammen)
Woka: Ein Avatar in einem virtuellen Treffpunkt, z.b. Workadventure, MMORPG
Qube: Eine Grundform, Cube, mit gewissen Grundeigenschaften, geeignet zur Kommunikationen zwischen Realitäten (Intra/Inter).

# Call for Qubes
*german & glossary above*

... tbd (Stuff in english)
