Respect @arl: Eine so große Runde beherzter Chaoswesen bei einem so polarisierendem Thema wie die Congress-Location zu moderieren, verdient schon ein "Herding-The-Cat" Exorbitant-Orden. Wenn ich das richtig Verstanden habe, dreht sich der Diskurs gleich um mehrere Themenkomplexe, weswegen die Debatte auch so "Mehrdimensional" ist:

1. Generell: Entscheidungsfindungen im volatilen Chaos - Wie kann sowas gelingen?
2. Konkret: MesseLeipzig oder CCH für 2022er Congress
3. Strategie: Wohin soll die CCC-Reise gehen? 
4. Wachstum oder Schrumpfen?

zu 1: (Generell)
Vielleicht hilft eine Kultur des nuudelns weiter? Hierzu könnte man im ersten Schritt die feine nuudle.digitalcourage.de mal anschauen. Die kann mehr als nur Terminfindung. Sollte das nicht gut genug skalieren, oder wollen wir komplexere Meinungsbilder erlangen könnten wir vielleicht was in Richtung Loomio (Open source collaborative decision making tool) überlegen. Loomio geht über eine reine Oneway Dialog-Abfrage aka MS-Form, FramaForms oder LimeSurvey hinaus.

zu 2: (Konkret)
Da schon ende Mai, braucht es, da sind wir uns glaub alle einig, eine schnelle Entscheidung für den 2022er Congress. Vermutlich reicht die Zeit da nicht für die notwendige, breitere und basisdemokratische Debatte über die Strategie. Deshalb vielleicht: Mit den gewonnenen Meinungen und Güterabwägungen von Gestern noch ein finales, simples "Leipzig vs. Hamburg" nuudle als Empfehlung ohne strikte Verbindlichkeit für die Verantwortungsträger:innen, evt. mit kurzem due-date: z.b. 23.6. o.ä. Zum Beispiel: https://nuudel.digitalcourage.de/EPG1aSQ6mn78TGMI 

zu 3 & 4: (Strategie)
Von hier an wird es schwierig, ggf. gefährlich, weil polarisierend mit teilweise gegenläufigen Meinungen, Empfindungen, Wünschen und Begehrlichkeiten. Aus den vielen Wortbeiträgen von Gestern abend, habe ich zwei Strömungen unterscheiden können:

... Bitte mehr Gemütlichkeit (Vom Chaos für's Chaos)
... Lasst uns das Chaos in die Welt tragen und die Welt zum Chaos einladen

Das ganze flankierend u.a. mit Aspekten der Nachhaltigkeit (u.a. Co2-Footprint, Recycling, Selbstausbeutung), Skalierbarkeit über die nächsten Jahre, Kreativ- & Sinnhaftigkeit.

Ganz Ehrlich? Glaub das braucht mehr Raum, mehr Zeit als ein Thread hier oder da. Das alleine wäre ein Loomio o.ä. wert.

Dann noch weitere 5 Cent zu den strategischen Themen, etwas orbitaler:

* Überlegen ob wir langfristig einen CCC nicht in verschiedenen Städten gastieren lassen könnten.  Glaub gäbe mehrere Städte die das gut gebrauchen könnten^^

* Tick-Tack Thinking (von Intel geklaut): Ein Team konzipiert ein kommendes Event, ein zweites gleich schon das Jahr danach. Weniger Streß der Art: "Oops-Folien-Sind-Noch-Nicht-Da". Das eröffnet gar einen 24 Monatigen Planungshorizont. 

* Central Orbit plus Satelites: Aktiv dezentrale Spaces und Bühnen VorOrt in das zentrale Congress-Konzept mit einbinden (könnte schöner skalieren)

* Metaverse, Multiverse, Fediverse, Di:verse als weitere "Hallen" denken. -> next Stop: Montag, 30.5. - 21:00 mumble.hamburg.ccc.de